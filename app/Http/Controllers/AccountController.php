<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use  App\Models\AccountApi;
//use Response;

class AccountController extends Controller
{
    //

	public function __construct() {
		$this->middleware('auth', ['except' => 'createPost']);
	}
    public function create(){

        if (view()->exists('frontend.customer.create')) {
            //
            return view('frontend.customer.create');
        }

    }

    /* Account Create  post Action */
    public function createPost(Request $request)
    {
		
        $validator = Validator::make($request->all(), [
            'emailId' => 'required|email',     // required and must be unique in the ducks table
            'userName' => 'required',
            'password' => 'required',
			//'mobileNumber' =>'digits_between:10,10'
            //'password_confirm' => 'required|same:password'
        ]);

        if ($validator->fails()) {
            return redirect('account/create')
                ->withErrors($validator)
                ->withInput();
        }
		
        $result=AccountApi::registeruser($request->all());
		if(!$result || $result == NULL ){
					\Session::flash('message','Customer Registraton failed ');
					\Session::flash('class', 'danger');
					return redirect('account/create');
		}
      	$resultAPi = json_decode($result);
		if(isset($resultAPi->errorCode) && $resultAPi->errorCode!=''){
					\Session::flash('message',$resultAPi->errorMessage);
					\Session::flash('class', 'danger');
					return redirect('account/create');
		}
        /*
        set a flag to account created and
          need  a otp verification
        */
		if(isset($resultAPi->responseCode) && ($resultAPi->responseCode==200)){
			
			
        if(\Session::has('mobile_verified')):
            \Session::forget('mobile_verified');
         endif;
		
		 $resultGenerateOTP=AccountApi::GenerateOTP($request->input('mobileNumber'));	
        \Session::put('mobileNumber',$request->input('mobileNumber'));
        \Session::put('mobile_verified',AccountApi::MOBILE_VERIFICATION_STATUS_INT);
		
		\Session::flash('message','Your Account are created ,Please verify Otp');
		\Session::flash('class', 'success');

		return redirect('account/VerifyOTP');
		}
    }

    /* Varify Customer mobile Otp */

    public function VerifyOTP(){
        /*
        * If mobile verification stage is init then otp this page
        */
        if(\Session::has('mobile_verified')
                && \Session::get('mobile_verified')==AccountApi::MOBILE_VERIFICATION_STATUS_INT){
            if (view()->exists('frontend.customer.verifyotp')) {
                //
                return view('frontend.customer.verifyotp');
            }
        }
    }
    /* Account profile   Action */
    public function profile(){
        if (view()->exists('frontend.customer.profile')) {
            //
            return view('frontend.customer.profile');
        }
    }
    /* Validate User by Generate OTP
    */
    public function VerifybyOTP(Request $request){

       if($request->has('oneTimePassword') &&
           \Session::get('mobile_verified')==AccountApi::MOBILE_VERIFICATION_STATUS_INT){

           return redirect()->route('account-profile');
       }
        return redirect()->route('account-VerifyOTP');

    }
	/* Save Profile data at api 
	*/
	public function SaveProfileData(Request $request){
		
	     $validator = Validator::make($request->all(), [
            'defaultEmailId' => 'required|email',     // required and must be unique in the ducks table
            'userName' => 'required',
            'password' => 'required',
			'defaultMobileNumber' =>'required|digits_between:10,10'
            //'password_confirm' => 'required|same:password'
        ]);

        if ($validator->fails()) {
            return redirect('account/profile')
                ->withErrors($validator)
                ->withInput();
        }
		
		/* Save Profile at API
		*/
		
        $result=AccountApi::Registeruser($request->all());
        
        $jArr = json_decode($result, true);
        
        if(isset($jArr['responseCode']) == 200) {
			dd($jArr);
		} else { 
			dd($jArr);
		}
        if(!$result):
            // Eror
	    endif;
        return redirect('account/profile');

	}

    /* Forgot password page */
    public function ChangePassword(){
        if (view()->exists('frontend.customer.changepassword')) {
            //
            return view('frontend.customer.changepassword');
        }
    }

    /* Forgot password save Action */

    public function ChangePasswordSave(Request $request){
        $validator = Validator::make($request->all(), [
            'password' => 'required',     // required and must be unique in the ducks table
            'newPassword' => 'required',
          ]);

        if ($validator->fails()) {
            return redirect('account/change-password')
                ->withErrors($validator)
                ->withInput();
        }

        /* Save Profile at API
        */

        $result=AccountApi::ChangePassword($request->all());
        if(!$result):
            // Eror
        endif;
        return redirect('account/profile');

    }

}
