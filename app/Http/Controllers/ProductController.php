<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ProductController extends Controller
{
   
   public function index() {
            return view('frontend.product.index');
    }

   
   
    public function getSearch(Request $request) {
		$inputs = $request->all();
		$selected_locality_lat=$inputs['selected_locality_lat'];
		$selected_locality_lng=$inputs['selected_locality_lng'];
		$configdata=$this->getConfigParameters();
		$productdata=$this->getProductList($selected_locality_lat,$selected_locality_lng);
		//echo '<pre>';
		//print_r($productdata);
		//exit;
		if($configdata->responseCode==200 && $productdata->responseCode==200){
			if(count($productdata->data)>0){
				$this->setData($configdata,$productdata);
				return redirect('products');
			}
			else{
				 \Session::flash('message', 'Oops! Product is not available In this location.'); 
				  return redirect('');
		      }
		}
		else{
		 \Session::flash('message', 'Oops! Service is not available in this location.'); 
		  return redirect('');
		}
		//\Session::flash('message', 'This is a message!'); 
		//\Session::flash('class', 'danger'); 
    }
	
	public function setData($configdata,$productdata) {
		$categorydata=$configdata->data;
		$displayCategories=$categorydata->eventDisplayCategories;
		$firstcategory=$displayCategories[0];
		$activeCategory=$firstcategory->id;
		$allproductdata=$productdata->data;
		$displayProductList=array();
		$displayProductDetails=array();
		$i=0;
		foreach($allproductdata as $onedata){
			$eventDisplayCategories=$onedata->eventDisplayCategories;
			$feedbackandreviews=$onedata->feedbackandreviews;
			$crossSalesItems=$onedata->crossSalesItems;
			$categoryFlag=0;
			if(count($eventDisplayCategories)>0){
				foreach($eventDisplayCategories as $oneCategory){
						$displayCategory=$oneCategory->displayCategory;
						if(count($displayCategory)){
							$displayCategoryId=$displayCategory->id;
							if($displayCategoryId==$activeCategory){
							 $categoryFlag=1;	
							}
					     }
				}
			}
			if($categoryFlag==1){
				$finalOfferedPrice=$onedata->finalOfferedPrice;
				$eventCuisine=$onedata->eventCuisine;
				$foodMenuItem=$eventCuisine->foodMenuItem;
				$displayProductList[$i]['id']=$foodMenuItem->id;
				$displayProductList[$i]['dataid']=$onedata->id;
				$displayProductList[$i]['image1Url']=$foodMenuItem->image1Url;
				if($i==0){
					$displayProductDetails['id']=$foodMenuItem->id;
					$displayProductDetails['note']=$eventCuisine->note;
					$displayProductDetails['finalOfferedPrice']=$finalOfferedPrice;
					$displayProductDetails['foodName']=$foodMenuItem->foodName;
					$displayProductDetails['image1Url']=$foodMenuItem->image1Url;
					$displayProductDetails['foodLongDescription']=$foodMenuItem->foodLongDescription;
					$displayProductDetails['foodShortDescription']=$foodMenuItem->foodShortDescription;
					$displayProductDetails['feedbackandreviews']=$feedbackandreviews;
					$displayProductDetails['crossSalesItems']=$crossSalesItems;
				}
			 $i++;	
			}	
		}
		\Session::put('categorydata', $categorydata);
		\Session::put('allproductdata', $allproductdata);
		\Session::put('displayCategories', $displayCategories);
		\Session::put('displayProductList', $displayProductList);
		\Session::put('displayProductDetails', $displayProductDetails);
		\Session::put('activeCategory', $activeCategory);
	}
    
	public function getProductList($lat,$lng) {
		$access_token=\Session::get('access_token');
	    //$url = 'http://feasthubtest.eu-west-1.elasticbeanstalk.com/api/v1.0/feasthub/search/event?lat='.$lat.'&lng='.$lng;
		$url = 'http://feasthubtest.eu-west-1.elasticbeanstalk.com/api/v1.0/feasthub/search/event?lat=17.4694857&lng=78.3587598';
		$headers = array(
			'Authorization:Bearer '.$access_token,
		  );
		$ch = curl_init();
		// Set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// Disabling SSL Certificate support temporarly
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// Execute post
		$result = curl_exec($ch);
		// Close connection
		curl_close($ch);
		//echo '<pre>';
		//print_r($result);
		//exit;
	    return json_decode($result);
	}
	
	public function getConfigParameters() {
		$access_token=\Session::get('access_token');
		$url = 'http://feasthubtest.eu-west-1.elasticbeanstalk.com/api/v1.0/feasthub/startup/config/params';
		$headers = array(
			'Authorization:Bearer '.$access_token,
		  );
		$ch = curl_init();
		// Set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// Disabling SSL Certificate support temporarly
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// Execute post
		$result = curl_exec($ch);
		// Close connection
		curl_close($ch);
		//echo '<pre>';
		//print_r($result);
		//exit;
	    return json_decode($result);
	}
	
	public function resetCategory(Request $request) {
		$result=array();
		$result['success']=false;
		$inputs = $request->all();
		$activeCategory=$inputs['catId'];
		$allproductdata=\Session::get('allproductdata');
		$displayProductList=array();
		$displayProductDetails=array();
		$i=0;
		foreach($allproductdata as $onedata){
			$eventDisplayCategories=$onedata->eventDisplayCategories;
			$feedbackandreviews=$onedata->feedbackandreviews;
			$crossSalesItems=$onedata->crossSalesItems;
			$categoryFlag=0;
			if(count($eventDisplayCategories)>0){
				foreach($eventDisplayCategories as $oneCategory){
						$displayCategory=$oneCategory->displayCategory;
						if(count($displayCategory)){
							$displayCategoryId=$displayCategory->id;
							if($displayCategoryId==$activeCategory){
							 $categoryFlag=1;	
							}
					     }
				}
			}
			if($categoryFlag==1){
				$finalOfferedPrice=$onedata->finalOfferedPrice;
				$eventCuisine=$onedata->eventCuisine;
				$foodMenuItem=$eventCuisine->foodMenuItem;
				$displayProductList[$i]['id']=$foodMenuItem->id;
				$displayProductList[$i]['dataid']=$onedata->id;
				$displayProductList[$i]['image1Url']=$foodMenuItem->image1Url;
				if($i==0){
					$displayProductDetails['id']=$foodMenuItem->id;
					$displayProductDetails['note']=$eventCuisine->note;
					$displayProductDetails['finalOfferedPrice']=$finalOfferedPrice;
					$displayProductDetails['foodName']=$foodMenuItem->foodName;
					$displayProductDetails['image1Url']=$foodMenuItem->image1Url;
					$displayProductDetails['foodLongDescription']=$foodMenuItem->foodLongDescription;
					$displayProductDetails['foodShortDescription']=$foodMenuItem->foodShortDescription;
					$displayProductDetails['feedbackandreviews']=$feedbackandreviews;
					$displayProductDetails['crossSalesItems']=$crossSalesItems;
				}
			  $i++;	
			}
			
		}
		if(count($displayProductList)>0 && count($displayProductDetails)>0){
			\Session::forget('displayProductList');
			\Session::forget('displayProductDetails');
			\Session::forget('activeCategory');
			\Session::put('displayProductList', $displayProductList);
			\Session::put('displayProductDetails', $displayProductDetails);
			\Session::put('activeCategory', $activeCategory);
			$result['success']=true;
		 }
		return \Response::json($result); 
    }
	
	public function resetProduct(Request $request) {
		$result=array();
		$result['success']=false;
		$inputs = $request->all();
		$dataId=$inputs['dataId'];
		$productId=$inputs['productId'];
		$allproductdata=\Session::get('allproductdata');
    	$displayProductDetails=array();
		foreach($allproductdata as $onedata){
			if($dataId==$onedata->id){
				$eventDisplayCategories=$onedata->eventDisplayCategories;
				$feedbackandreviews=$onedata->feedbackandreviews;
				$crossSalesItems=$onedata->crossSalesItems;
				$finalOfferedPrice=$onedata->finalOfferedPrice;
				$eventCuisine=$onedata->eventCuisine;
				$foodMenuItem=$eventCuisine->foodMenuItem;
				$displayProductDetails['id']=$foodMenuItem->id;
				$displayProductDetails['note']=$eventCuisine->note;
				$displayProductDetails['finalOfferedPrice']=$finalOfferedPrice;
				$displayProductDetails['foodName']=$foodMenuItem->foodName;
				$displayProductDetails['image1Url']=$foodMenuItem->image1Url;
				$displayProductDetails['foodLongDescription']=$foodMenuItem->foodLongDescription;
				$displayProductDetails['foodShortDescription']=$foodMenuItem->foodShortDescription;
				$displayProductDetails['feedbackandreviews']=$feedbackandreviews;
				$displayProductDetails['crossSalesItems']=$crossSalesItems;
				if(count($displayProductDetails)>0){
					\Session::forget('displayProductDetails');
					\Session::put('displayProductDetails', $displayProductDetails);
					$result['success']=true;
				 }
			}
        }
		return \Response::json($result); 
      }
}
