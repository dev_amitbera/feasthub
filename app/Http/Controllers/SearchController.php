<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Http\Requests;

class SearchController extends Controller
{
    /**
     * Show the application Search Page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.search');
    }
}
