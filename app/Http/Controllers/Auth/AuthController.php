<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;
use  App\Models\AccountApi;

class AuthController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Registration & Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users, as well as the
      | authentication of existing users. By default, this controller uses
      | a simple trait to add these behaviors. Why don't you explore it?
      |
     */

use AuthenticatesAndRegistersUsers,
    ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/account/profile';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
     
     public function __construct(Guard $auth, User $user) {
        $this->user = $user;
        $this->auth = $auth;
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        return Validator::make($data, [
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data) {
		
		
        return User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
        ]);
    }
    public function postRegister(Request $request) {
		$data = $request->all();
		$data['email'] = $data['defaultEmailId'];
		$validator = Validator::make($data, [
            'userName' => 'required|unique:users',
			'defaultEmailId' => 'required|unique:users',
			'email' => 'required|unique:users',
			'defaultMobileNumber' => 'required',
        ]);	
		
		$response = AccountApi::registeruser($data);

		
		if(isset($response['data']) && is_array($response['data'])) {
			
			$response['data']['name'] = $data['userName'];
			$response['data']['email'] = $data['defaultEmailId'];
			$response['data']['pwd'] = $response['data']['password'];
			$response['data']['password'] = bcrypt($response['data']['password']);
			
			//dd($response['data']);
			$object = new User;
			$object->fill($response['data']);
			
			if ($validator->fails()) {
			return redirect('register')
				->withErrors($validator)
				->withInput();
			}
			User::create($response['data']);
			
		} else {
			return redirect('register')
				->withErrors($response)
				->withInput();
		}
		
        return redirect()->route('login');
        
	}
    public function getRegister() {
		if (view()->exists('auth.create')) {
            return view('auth.create');
        }
	}
	
	public function getLogin() {
		if (view()->exists('auth.login')) {
            return view('auth.login');
        }
	}
	
	
	public function postLogin(Request $request) {
		$data = $request->all();
		
		$validator = Validator::make($request->all(), [
            'email' => 'required',
			'password' => 'required'
        ]);
        if ($validator->fails()) {
			return redirect('login')
				->withErrors($validator)
				->withInput();
        }
        // api login
        
        $result = AccountApi::login($data);
        
        if($result) {
			if (\Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
				return redirect()->route('account-profile');
			} else {
				return redirect('login')->withErrors([
						'email' => 'Incorrect username or password.',
				]);
			}
		} else {
			return redirect('login')->withErrors([
                'email' => 'Incorrect username or password.',
            ]);
		}
			
	}
	
	
	public function getlogout() {		
		$this->auth->logout();              		
		return redirect('login');
	}
}
