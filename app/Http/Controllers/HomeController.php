<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller {

    /**
     * Show the application search page
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
		
		\Session::forget('access_token'); 
		
		if (!\Session::has('access_token')) {
           $url = 'http://feasthubtest.eu-west-1.elasticbeanstalk.com/oauth/token?grant_type=client_credentials&client_id=client1&client_secret=client1&scope=read_myapp';
		    $ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			$result = curl_exec($ch);
			curl_close($ch);
			
			$decodedresult=json_decode($result);
			
			if(!empty($decodedresult)) {
				foreach($decodedresult as $key=>$value) {
				\Session::put($key, $value);
				}
			}
		}
		return view('frontend.home');
    }

}
