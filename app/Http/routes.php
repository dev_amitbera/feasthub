<?php

/*
  |--------------------------------------------------------------------------
  | Routes File
  |--------------------------------------------------------------------------
  |
  | Here is where you will register all of the routes in an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */


/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | This route group applies the "web" middleware group to every route
  | it contains. The "web" middleware group is defined in your HTTP
  | kernel and includes session state, CSRF protection, and more.
  |
 */

Route::group(['middleware' => ['web']], function () {
    //

	Route::get('login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
	Route::post('login', ['as' => 'login', 'uses' => 'Auth\AuthController@postLogin']);
    
    
	Route::get('/logout', 'Auth\AuthController@getLogout');
	Route::get('register', 'Auth\AuthController@getRegister');
	Route::post('/register', 'Auth\AuthController@postRegister'); 
  
	Route::get('/',array('as' => 'home', 'uses' => 'HomeController@index'));
    Route::get('/products', 'ProductController@index');
    Route::post('/getSearch', 'ProductController@getSearch');
	Route::post('/reset-category', 'ProductController@resetCategory');
	Route::post('/reset-product', 'ProductController@resetProduct');

    Route::get('account/create', ['as' => 'account-create', 'uses' => 'AccountController@create']);
    Route::post('account/createPost', ['as' => 'account-createPost', 'uses' => 'AccountController@createPost']);
 
    Route::get('account/profile', ['as' => 'account-profile', 'uses' => 'AccountController@profile']);
    Route::post('account/saveprofiledata', ['as' => 'account-saveprofiledata', 'uses' => 'AccountController@SaveProfileData']);

    /* OTP Varification */
    Route::get('account/VerifyOTP', ['as' => 'account-VerifyOTP', 'uses' => 'AccountController@VerifyOTP']);
    Route::post('account/verifybyotp', ['as' => 'account-verifybyotp', 'uses' => 'AccountController@VerifybyOTP']);

    /* forgot password Section */

    Route::get('account/change-password', ['as' => 'account-ChangePassword', 'uses' => 'AccountController@ChangePassword']);
    Route::post('account/change-password-save', ['as' => 'account-ChangePasswordSave', 'uses' => 'AccountController@ChangePasswordSave']);




});

Route::group(['middleware' => 'web'], function () {
    //Route::auth();

    Route::get('/home', 'HomeController@index');

    // route to show the login form
    Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function() {
        Route::get('/', array('as' => 'admin', 'uses' => 'DashboardController@index'));
    });
});
