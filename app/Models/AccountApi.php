<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Mockery\CountValidator\Exception;

class AccountApi extends Model
{

	protected $fillable = ['firstName', 'middleName', 'lastName', 'defaultEmailId', 'userName', 'defaultMobileNumber', 'password','gender', 'api_password'];

    const MOBILE_VERIFICATION_STATUS_INT='int';

    const MOBILE_VERIFICATION_STATUS_DONE='done';

    const MOBILE_VERIFICATION_STATUS_PROCESSING='PROCESSING';
    const API_DOMAIN_URL='http://feasthubtest.eu-west-1.elasticbeanstalk.com/api/v1.0/feasthub/';	
    /*  Generate OTP
      * Param @var $Mobileno  string
       * Return  string
    */
    public  static function GenerateOTP($Mobileno){

		$accessToken= \Session::get('access_token');
		 $url=self::API_DOMAIN_URL."otp/generateOTP?access_token=".$accessToken;
		$ch = curl_init( $url );
		unset($data['_token']);
		$postparams = json_encode($data);
		print_r($postparams);
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $postparams );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		$result = curl_exec($ch);
         curl_close($ch);
		return $result;
    }

    /* Account registration  at apI
    */
     public static  function registeruser($data){
		
		$params['mobileNumber']  = $data['defaultMobileNumber'];
		$params['userName']  = $data['userName'];
		$params['emailId']  = $data['defaultEmailId'];
		$params['password']  = $data['password'];
		
		$accessToken= \Session::get('access_token');
		$url=self::API_DOMAIN_URL."registrations/registeruser?access_token=".$accessToken;
		$ch = curl_init( $url );
		unset($data['_token']);
		$postparams = json_encode($params);
		
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $postparams );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		$result = curl_exec($ch);
		curl_close($ch);
		//echo '<pre>'; 
		//print_r($result);
		//echo '</pre>';	
		return json_decode($result, true);
 
     }
     
     
     
     /*
      * 
      * 
      */
      
      
	public static function login($data) {

		$email = 'amitbera'; //$data['email'];
		$password = 'amitbera'; //$data['password'];
		$apiResult = [];

		$url = 'http://feasthubtest.eu-west-1.elasticbeanstalk.com/oauth/token?grant_type=password&client_id=restapp&client_secret=restapp&username='. $email .'&password='.$password.'&scope=feasthub';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$result = curl_exec($ch);
		curl_close($ch);
		$decodedresult  = json_decode($result);
		
		if(!empty($decodedresult)) {
			foreach($decodedresult as $key=>$value) {
			\Session::put($key, $value);
			}
		}
		if(empty($decodedresult->access_token)) {
			
			return false;
		}
		return true;
	}
}
