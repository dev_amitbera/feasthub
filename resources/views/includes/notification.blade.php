<section id="notification">
	<div id="noty-holder"></div><!-- HERE IS WHERE THE NOTY WILL APPEAR-->
</section>
@if(Session::has('message'))
	<script>
		$(function(){
		createNoty('{{ \Session::get('message') }}', '{{ \Session::get("class") }}');
		
		$('.page-alert .close').click(function(e) {
			e.preventDefault();
			$(this).closest('.page-alert').slideUp();
		});
	});
	</script>	
@endif

@if(Session::has('success'))
	<script>
		$(function(){
		createNoty('{{ \Session::get('success') }}', '{{ \Session::get("class") }}');
		
		$('.page-alert .close').click(function(e) {
			e.preventDefault();
			$(this).closest('.page-alert').slideUp();
		});
	});
	</script>	
@endif
@if(Session::has('warning'))
	<script>
		$(function(){
		createNoty('{{ \Session::get('warning') }}', '{{ \Session::get("class") }}');
		
		$('.page-alert .close').click(function(e) {
			e.preventDefault();
			$(this).closest('.page-alert').slideUp();
		});
	});
	</script>	
@endif
