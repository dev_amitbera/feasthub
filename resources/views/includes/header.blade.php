<header>
    <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
        <ul class="nav sidebar-nav">
            <li class="sidebar-brand"> <a href="#"> Menu </a> </li>
            <li> <a href="#">Home</a> </li>
            <li> <a href="#">About</a> </li>
            <li> <a href="#">Blog</a> </li>
            <li> <a href="#">Careers</a> </li>
            <!--<li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Works <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li class="dropdown-header">Dropdown heading</li>
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>-->
            <li> <a href="#">Contact</a> </li>
            <li> <a href="#">Follow me</a> </li>
        </ul>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-sm-4 left-nav">
                <button type="button" class="hamburger is-closed" data-toggle="offcanvas"> <span class="hamb-top"></span> <span class="hamb-middle"></span> <span class="hamb-bottom"></span> </button>
            </div>
            <div class="col-sm-4 top-middle"><a href="#" class="logo">FeastHUB</a></div>
            <!--<div class="col-sm-4 top-right"><a href="#" class="login"><i class="fa fa-lock"></i> Login</a></div>-->
        </div>
    </div>
    <div class="clearfix"></div>
</header>
