<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<meta name="viewport" content="user-scalable=no, width=1390">-->
        <title>List</title>

        <!-- Stylesheets Desktop -->
        <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('frontend/css/font-awesome.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('frontend/css/main.css') }}" />
        <link rel="stylesheet" href="{{ asset('frontend/css/side-navbar.css') }}" />
        <link rel="stylesheet" href="{{ asset('frontend/css/responsive.css') }}" />
        <link rel="stylesheet" href="{{ asset('frontend/css/notification.css') }}" />
        <link rel="stylesheet" href="{{ asset('frontend/css/elastislide.css') }}" />
        <script src="{{ asset('frontend/js/modernizr.custom.17475.js') }}"></script>
        <script src="{{ asset('frontend/js/jquery-2.1.4.min.js') }}"></script>
        <script>
$(document).ready(function () {
    $(".nav-tabs a").click(function () {
        $(this).tab('show');
    });
    $('.nav-tabs a').on('shown.bs.tab', function (event) {
        var x = $(event.target).text();         // active tab
        var y = $(event.relatedTarget).text();  // previous tab
        $(".act span").text(x);
        $(".prev span").text(y);
    });
});
</script>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div id="wrapper">
            @include('frontend.include.header')
            @include('includes.notification')
            @yield('content')
            @include('includes.footer')         
            @include('includes.bottom_footer')
        </div>
    </div>
    
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script> 
    <script src="{{ asset('frontend/js/main.js') }}"></script>
    <script src="{{ asset('frontend/js/notification.js') }}"></script>
    <script src="{{ asset('frontend/js/jquerypp.custom.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.elastislide.js') }}"></script>
    <script type="text/javascript">
			$( '#carousel' ).elastislide( {
				orientation : 'vertical'
			} );
			
		</script>
</body>
</html>
