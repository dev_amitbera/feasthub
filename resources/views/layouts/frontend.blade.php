<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<meta name="viewport" content="user-scalable=no, width=1390">-->
        <title>Welcome Feasthub</title>

        <!-- Stylesheets Desktop -->

        <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('frontend/css/font-awesome.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('frontend/css/main.css') }}" />
        <link rel="stylesheet" href="{{ asset('frontend/css/side-navbar.css') }}" />
        <link rel="stylesheet" href="{{ asset('frontend/css/responsive.css') }}" />
        <link rel="stylesheet" href="{{ asset('frontend/css/notification.css') }}" />
        <script src="{{ asset('frontend/js/jquery-2.1.4.min.js') }}"></script>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <div id="wrapper">

            @include('frontend.include.header')
            @include('includes.notification')
            @yield('content')
            @include('includes.footer')         
            @include('includes.bottom_footer')
            
        </div>
        <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script> 
        <script src="{{ asset('frontend/js/notification.js') }}"></script>
        <script>
$(document).ready(function () {
    var trigger = $('.hamburger'), overlay = $('.overlay'), isClosed = false;
    trigger.click(function () {
        hamburger_cross();
    });
    function hamburger_cross() {
        if (isClosed == true) {
            overlay.hide();
            trigger.removeClass('is-open');
            trigger.addClass('is-closed');
            isClosed = false;
        } else {
            overlay.show();
            trigger.removeClass('is-closed');
            trigger.addClass('is-open');
            isClosed = true;
        }
    }
    $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
    });
});
        </script>
        <script>
            if ($('#back-to-top').length) {
                var scrollTrigger = 100, // px
                        backToTop = function () {
                            var scrollTop = $(window).scrollTop();
                            if (scrollTop > scrollTrigger) {
                                $('#back-to-top').addClass('show');
                            } else {
                                $('#back-to-top').removeClass('show');
                            }
                        };
                backToTop();
                $(window).on('scroll', function () {
                    backToTop();
                });
                $('#back-to-top').on('click', function (e) {
                    e.preventDefault();
                    $('html,body').animate({
                        scrollTop: 0
                    }, 700);
                });
            }
        </script>
    </body>
</html>
