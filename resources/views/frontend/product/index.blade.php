<?php 
	$displayCategories=\Session::get('displayCategories');
	$displayProductList=\Session::get('displayProductList');
	$displayProductDetails=\Session::get('displayProductDetails');
	$activeCategory=\Session::get('activeCategory');
	$allproductdata=\Session::get('allproductdata');
?>

@extends('layouts.productlist')

@section('content')
<section class="product-category">
  		<div class="container">
        	<div class="row">
                 @if (count($displayCategories))
                    <ul>
                       @foreach($displayCategories as $singleCategory)
                         @if ($singleCategory->displayFlag==1)
                            <li>
                            <a href="#" class="category-list" data-category-id="{{ $singleCategory->id }}" data-category-url="{{ url('/reset-category') }}" 
                            data-token="{!! csrf_token() !!}">
                                    <span class="thumb">
                                        {{str_replace("_"," ","$singleCategory->displayCategoryName")}}
                                    </span>
                             </a>
                            </li>
                          @endif   
                        @endforeach
                    </ul>
                 @else
                    <p>No category exists.</p>
                 @endif             
  			</div>
  		</div>
 </section>
<section class="product-area">
    <div class="container">
        <div class="row">
         <div id="ninja-slider" class="col-sm-9 slider-right">
          <div class="slider-inner">
            <ul class="min-height">
              <li>
                @if (count($displayCategories))
                <div class="product-details">
                  <div class="col-sm-4">
                    <img src="{{ $displayProductDetails['image1Url'] }}" alt="" title=""/>
                    <p style="margin:6px 0;">{{ $displayProductDetails['note'] }}</p>
                  </div>
                  <div class="col-sm-8 details-right">
                    <div class="title-left">
                      <h2>{{ $displayProductDetails['foodName'] }}</h2>
                    </div>
                    <div class="icon-right"><a href="javascript:void(0)"><i class="fa fa-heart"></i></a></div>
                    <div class="clearfix"></div>
                    <h3><i class="fa fa-inr"></i>{{ $displayProductDetails['finalOfferedPrice'] }}</h3>
                    <div class="center">
                      <div class="input-group"> 
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]"> 
                              <span class="glyphicon glyphicon-minus"></span> 
                            </button>
                        </span>
                        <input type="text" class="form-control input-number text-center" value="0">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]"> 
                              <span class="glyphicon glyphicon-plus"></span> 
                            </button>
                        </span> 
                      </div>
                    </div>
                    <div class="calander-icon"><i class="fa fa-calendar"></i></div>
                  </div>
                </div>
                @else
                    <p>No product information exists.</p>
                 @endif 
                <div class="clearfix"></div>
                
                <div class="details-tabs">
                
                  <div class="clearfix"></div>
                  
                  <div class="row">
                    <div class="col-md-12">
                      <div class="panel with-nav-tabs panel-default">
                        <div class="panel-heading">
                          <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1default" data-toggle="tab">Detail</a></li>
                            <li><a href="#tab2default" data-toggle="tab">Nutrition</a></li>
                            <li><a href="#tab3default" data-toggle="tab">Reviews</a></li>
                            <li><a href="#tab4default" data-toggle="tab">Tags</a></li>
                          </ul>
                        </div>
                        <div class="panel-body">
                          <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab1default">
                              <p>{{ $displayProductDetails['foodLongDescription'] }}</p>
                              <div class="clearfix"></div>
                              <hr/>
                              <div class="details-table">
                                <table class="table table-bordered">
                                  <tbody>
                                    <tr>
                                      <td>02</td>
                                      <td><a href="#"><i class="fa fa-angle-left fa-ico"></i></a> Tanduri Roti <a href="#"><i class="fa fa-angle-right fa-ico"></i></a></td>
                                      <td><i class="fa fa-inr"></i> 20.00</td>
                                    </tr>
                                    <tr>
                                      <td>01</td>
                                      <td><a href="#"><i class="fa fa-angle-left fa-ico"></i></a> Veg Pulao <a href="#"><i class="fa fa-angle-right fa-ico"></i></a></td>
                                      <td><i class="fa fa-inr"></i> 15.00</td>
                                    </tr>
                                    <tr>
                                      <td>01</td>
                                      <td>Dal</td>
                                      <td></td>
                                    </tr>
                                    <tr>
                                      <td>02</td>
                                      <td>Vegetable Curry</td>
                                      <td></td>
                                    </tr>
                                    <tr>
                                      <td>01</td>
                                      <td>Curd</td>
                                      <td></td>
                                    </tr>
                                    <tr>
                                      <td>01</td>
                                      <td>Green Salad</td>
                                      <td></td>
                                    </tr>
                                    <tr>
                                      <td>01</td>
                                      <td>Pickle</td>
                                      <td></td>
                                    </tr>
                                    <tr>
                                      <td>01</td>
                                      <td>Sweet</td>
                                      <td></td>
                                    </tr>
                                  </tbody>
                                </table>
                                <div class="clearfix"></div>
                              </div>
                              <div class="clearfix"></div>
                            </div>
                            
                            <div class="tab-pane fade" id="tab2default" style="text-align:center;"> Coming Soon</div>
                            
                            <div class="tab-pane fade" id="tab3default">
                              <div class="row">
                                <div class="col-sm-4 AT3RevSummary text-center">
                                  <div class="AT3RevLarge"> 3.4 </div>
                                  <div style="font-size:20px;"> 
                                    <span class="glyphicon glyphicon-star"></span> 
                                    <span class="glyphicon glyphicon-star"></span> 
                                    <span class="glyphicon glyphicon-star"></span> 
                                    <span class="glyphicon glyphicon-star-empty"></span> 
                                    <span class="glyphicon glyphicon-star-empty"></span> 
                                  </div>
                                  <div style="color:#ccc;"><i class="fa fa-user"></i><span style="font-weight:300"> 32,208,186</span> total </div>
                                </div>
                                
                                <div class="col-sm-8 AT3RevSummary">
                                
                                  <div class="m-bottom">
                                    <div class="col-sm-4"> 
                                        <span class="glyphicon glyphicon-star"></span> 
                                        <span class="glyphicon glyphicon-star"></span> 
                                        <span class="glyphicon glyphicon-star"></span> 
                                        <span class="glyphicon glyphicon-star"></span> 
                                        <span class="glyphicon glyphicon-star"></span> 
                                    </div>
                                    
                                    <div class="col-sm-8">
                                     <span style="font-weight:300; color:#000; width:100%; display: block; background:#21A700; padding:0 6px;"> 18,402,301</span>
                                     </div>
                                  </div>
                                  
                                  <div class="clearfix"></div>
                                  
                                  <div class="m-bottom">
                                    <div class="col-sm-4"> 
                                     <span class="glyphicon glyphicon-star"></span> 
                                     <span class="glyphicon glyphicon-star"></span> 
                                     <span class="glyphicon glyphicon-star"></span> 
                                     <span class="glyphicon glyphicon-star"></span> 
                                     <span class="glyphicon glyphicon-star-empty"></span> 
                                    </div>
                                    <div class="col-sm-8">
                                      <span style="font-weight:300; color:#000; width:60%; display: block; background:#40E463; padding:0 6px;"> 5,090,822</span>
                                    </div>
                                  </div>
                                  
                                  <div class="clearfix"></div>
                                  
                                  <div class="m-bottom">
                                    <div class="col-sm-4"> 
                                      <span class="glyphicon glyphicon-star"></span> 
                                      <span class="glyphicon glyphicon-star"></span> 
                                      <span class="glyphicon glyphicon-star"></span> 
                                      <span class="glyphicon glyphicon-star-empty"></span> 
                                      <span class="glyphicon glyphicon-star-empty"></span> 
                                    </div>
                                    <div class="col-sm-8">
                                      <span style="font-weight:300; color:#000; width:46%; display: block; background:#F0FF08; padding:0 6px;"> 3,022,007</span>
                                    </div>
                                  </div>
                                  
                                  <div class="clearfix"></div>
                                  
                                  <div class="m-bottom">
                                    <div class="col-sm-4"> 
                                      <span class="glyphicon glyphicon-star"></span> 
                                      <span class="glyphicon glyphicon-star"></span> 
                                      <span class="glyphicon glyphicon-star-empty"></span> 
                                      <span class="glyphicon glyphicon-star-empty"></span> 
                                      <span class="glyphicon glyphicon-star-empty"></span> 
                                    </div>
                                    <div class="col-sm-8"> 
                                      <span style="font-weight:300; color:#000; width:26%; display: block; background:#C5AF0D; padding:0 6px;"> 1,574,506</span>
                                    </div>
                                  </div>
                                  
                                  <div class="clearfix"></div>
                                  
                                  <div class="m-bottom">
                                    <div class="col-sm-4"> 
                                       <span class="glyphicon glyphicon-star"></span> 
                                       <span class="glyphicon glyphicon-star-empty"></span> 
                                       <span class="glyphicon glyphicon-star-empty"></span> 
                                       <span class="glyphicon glyphicon-star-empty"></span> 
                                       <span class="glyphicon glyphicon-star-empty"></span> 
                                    </div>
                                    <div class="col-sm-8"> 
                                       <span style="font-weight:300; color:#000; width:34%; display: block; background:#F63; padding:0 6px;"> 4,112,908</span>
                                    </div>
                                  </div>
                                  
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              <br/>
                              <div class="clearfix"></div>
                              <br/>
                              <div class="col-sm-12 client-review">
                                <div class="col-sm-2 review-img"><img src="{{ asset('frontend/img/avatar.png') }}" alt=""/></div>
                                <div class="col-sm-10">
                                  <h3 class="pull-left">Alan Ward</h3>
                                  <h6 class="pull-right" style="font-weight:300;">January 29, 2016</h6>
                                  <div class="clearfix"></div>
                                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s......<br/>
                                    <a href="#" style="color:#900;">view more</a> </p>
                                </div>
                              </div>
                              <div class="col-sm-12 client-review">
                                <div class="col-sm-2 review-img"><img src="{{ asset('frontend/img/avatar.png') }}" alt=""/></div>
                                <div class="col-sm-10">
                                  <h3 class="pull-left">Justin Ruiz</h3>
                                  <h6 class="pull-right" style="font-weight:300;">January 29, 2016</h6>
                                  <div class="clearfix"></div>
                                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s......<br/>
                                    <a href="#" style="color:#900;">view more</a> </p>
                                </div>
                              </div>
                            </div>
                            
                            <div class="tab-pane fade" id="tab4default" style="text-align:center;"> Coming Soon</div>
                          </div>
                        </div>
                      </div>
                      
                      <div class="clearfix"></div>
                      
                      @if (count($displayProductDetails['crossSalesItems']))
                      
                      <div class=''>
                        <div class='col-md-12 similar-item'>
                          <h3>You may also like to add these items</h3>
                          <div class="carousel slide media-carousel" id="media">
                            <div class="carousel-inner">
                             <?php $pos=0 ;?>
                             <?php $crossSalesItems=$displayProductDetails['crossSalesItems'] ;?>
                             <?php foreach($crossSalesItems as $crossSalesItem):?>
                             <?php $foodMenuItemView=$crossSalesItem->foodMenuItemView;?>
                             <?php if($pos==0 || $pos/3==0||(count($crossSalesItems)-$pos)<3):?>
                              <div class="item <?php if($pos==0){?>active<?php } ?>">
                                <div class="row">
                                <?php endif ?>
                                  <div class="col-md-4"> 
                                     <a class="thumbnail" href="#">
                                      <img alt="" src="<?php echo $foodMenuItemView->image1Url;?>">
                                       <h4><?php echo $foodMenuItemView->foodName;?></h4>
                                    </a> 
                                  </div>
                               <?php if(($pos+1)/3==0||(count($crossSalesItems)-$pos)==1):?>
                                </div>
                              </div>
                              <?php endif ;?>
                              <?php $pos++; ?>
                             <?php endforeach ;?>
                            </div>
                            <a data-slide="prev" href="#media" class="left carousel-control">‹</a> 
                            <a data-slide="next" href="#media" class="right carousel-control">›</a> 
                          </div>
                        </div>
                      </div>
                      
                      @endif 
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
              </li>
            </ul>
          </div>
          <div class="clearfix"></div>
        </div>
        
         <div id="thumbnail-slider" class="col-sm-3 slider-left">
                <div class="inner">
                 @if (count($displayProductList))
                    <ul>
                       @foreach($displayProductList as $singleProduct)
                        <li>
                            <a href="#" class="product-list" data-id="{{ $singleProduct['dataid'] }}" data-product-id="{{ $singleProduct['id'] }}" 
                            data-product-url="{{ url('/reset-product') }}" data-token="{!! csrf_token() !!}">
                                <span class="thumb" style="background-image:url({{ $singleProduct['image1Url'] }})">
                                </span>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                 @else
                    <p>No Products exist.</p>
                 @endif                 
              </div>
          </div>
          
          <div class="col-sm-3 slider-left">
          	<div class="demo-2">
             <div class="main clearfix">
				<div class="column">
					<!-- Elastislide Carousel -->
                     @if (count($displayProductList))
                     <ul id="carousel" class="elastislide-list">
                       @foreach($displayProductList as $singleProduct)
                        <li>
                            <a href="#" class="product-list" data-id="{{ $singleProduct['dataid'] }}" data-product-id="{{ $singleProduct['id'] }}" 
                            data-product-url="{{ url('/reset-product') }}" data-token="{!! csrf_token() !!}">
                                <img src="{{ $singleProduct['image1Url'] }}" alt="" />
                            </a>
                        </li>
                        @endforeach
                     </ul>
                     @else
                        <p>No Products exist.</p>
                     @endif      
					<!-- End Elastislide Carousel -->
				</div>
			 </div>
		    </div>
          </div>
          
         <div class="clearfix"></div>
        </div>
    </div>    
</section>
@endsection
		
