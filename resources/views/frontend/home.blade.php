@extends('layouts.frontend')

@section('content')
<section class="jk-slider">
    <div id="carousel-example" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item active"><img src="{{ asset('frontend/img/slider-1.jpg') }}" alt=""/></div>
            <div class="item"><img src="{{ asset('frontend/img/slider-2.jpg') }}" alt=""/></div>
        </div>
    </div>
	<script src="http://maps.googleapis.com/maps/api/js?libraries=places" type="text/javascript"></script>
    <script src="{{ asset('frontend/js/search.js') }}"></script>
    <div class="searchContainer">
        <div class="searchInnerContainer">
            <div class="orderHomePageCTA">
                <h3>Ready to begin your food journey? </h3>
            </div>
            <div class="search">
                <div class="row">
                    {!! Form::open(['url'=> url('/getSearch'),'name'=>'searchForm', 'method'=>'POST', 'id'=>'searchForm', 'class'=>"form-inline"]) !!}
                        <div class="col-sm-3">
                        {!! 
                             Form::select('city', 
                                            array(
                                                    '' =>'Please select city',
                                                    'Mumbai' => 'Mumbai', 
                                                    'Bangalore' => 'Bangalore',
                                                    'Pune' => 'Pune',
                                                    'Hyderabad' => 'Hyderabad',
                                                    'Coimbatore' => 'Coimbatore',
                                                    'Vadodara' => 'Vadodara',
                                                    'Chennai' => 'Chennai',
                                                    'Ahmedabad' => 'Ahmedabad',
                                                    'Nagpur' => 'Nagpur',
                                                    'Indore' => 'Indore',
                                                    'Bhopal' => 'Bhopal',
                                                    'Gurgaon' => 'Gurgaon',
                                                    'New Delhi' => 'New Delhi'    
                                                  ), 
                                            '',
                                            ['id' => 'city', 'class' => 'selectCity']
                                         )
                         !!}
              
                        </div>
                        <div class="col-sm-6">
                        {!! Form::macro('searchField', function()
                        {
                            return '<input type="search" placeholder="Type your Delivery location ( landmark, road, area)" class="locality"  id="searcharea"/>';
                        });
                        !!}
                        {!! Form::searchField() !!}
                        </div>
                        <div class="col-sm-3">
                         {!! Form::button('Load Menu', ['class'=>'btn black','id'=>'load_menu']) !!}
                        </div>
                        {!! Form::hidden('selected_locality_name', '',['id' => 'selected_locality_name']) !!}
                        {!! Form::hidden('selected_locality_lat', '',['id' => 'selected_locality_lat']) !!}
                        {!! Form::hidden('selected_locality_lng', '',['id' => 'selected_locality_lng']) !!}
                        
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
