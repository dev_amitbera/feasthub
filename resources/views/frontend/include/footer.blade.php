<?php
/**
 * Created by PhpStorm.
 * User: Amit
 * Date: 6/23/2016
 * Time: 11:30 PM
 */
 ?>
 <footer>
    <div class="container">
      <div class="row">
        <div class="col-sm-4 footer-1">
          <h3>The Feast Hub</h3>
          <ul class="menu-footer">
            <li><a href="#">Our Food</a></li>
            <li><a href="#">Code of conduct</a></li>
            <li><a href="#">Privacy Policy</a></li>
            <li><a href="#">Disclaimer</a></li>
            <li><a href="#">Terms &amp; Conditions</a></li>
          </ul>
        </div>
        <div class="col-sm-4 footer-2">
          <h3>Connect with Us</h3>
          <div class="social">
            <ul>
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            </ul>
          </div>
          <ul class="menu-footer">
            <li><a href="#">The FeastHUB Blog</a></li>
            <li><a href="#">Contact us</a></li>
          </ul>
        </div>
        <div class="col-sm-4 footer-3">
          <h3>Download our app at</h3>
          <div class="textwidget">
            <div class="row"> <a href="#" class="appDiv" style="padding-right: 62px;"> <img src="{{ asset('frontend/img/icon-ios.png') }}">Available on the App Store</a> <a href="#" class="appDiv" style="padding-right: 34px;"> <img src="{{ asset('frontend/img/Android-icon.png') }}">Available on the Android Store</a> <a href="#" class="appDiv"><img src="{{ asset('frontend/img/google-Play-1.png') }}">Available on the Google Play Store</a> </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
    <a class="go-top show" id="back-to-top"><i class="fa fa-angle-up"></i></a>
  <div class="site-footer">
    <div class="site-info container"><a href="#">Contact us at 000.0000.0000</a> <span class="sep">| </span><a href="#">Feasthub copyright.</a></div>
  </div>