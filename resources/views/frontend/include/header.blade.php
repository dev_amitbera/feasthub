<?php
/**
 * User: Amit
 * Date: 6/23/2016
 * Time: 11:28 PM
 */
?>

<header>
    <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
      <ul class="nav sidebar-nav">
        <li class="sidebar-brand"> </li>
        @if (Auth::guest())
			<li class="sidebar-brand"><a href="{{ url('/login') }}">Login</a></li>
			<li class="sidebar-brand"><a href="{{ url('/register') }}">Register</a></li>
		@else 
			<li><a href="{{ url('/logout') }}">Logout</a></li>
		@endif
        
        <li> <a href="{{ route('account-profile')}}">Profile</a> </li>
        <li> <a href="{{ route('account-ChangePassword')}}">Change Password</a> </li>
        <li> <a href="#">Payment</a> </li>
        <li> <a href="#">Orders</a> </li>
        <li> <a href="#">{{ \Session::get('access_token')}}</a> </li>
      </ul>
    </nav>
    <div class="container">
      <div class="row">
        <div class="col-sm-4 left-nav">
          <button type="button" class="hamburger is-closed" data-toggle="offcanvas"> <span class="hamb-top"></span> <span class="hamb-middle"></span> <span class="hamb-bottom"></span> </button>
        </div>
        <div class="col-sm-4 top-middle"><a href="{{ route('home') }}" class="logo">FeastHUB</a></div>
        <!--<div class="col-sm-4 top-right"><a href="javascript:void(0)" class="login"><i class="fa fa-lock"></i> Login</a></div>--> 
        
      </div>
    </div>
    <div class="clearfix"></div>
  </header>
 
