@extends('frontend/layout/1-column')
@section('content')
<!-- start of profile edit page -->

<section class="product-area">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 account-area">
          <h2 class="order-text">Create an Account</h2>
          <div class="clearfix"></div>
          <!-- start of Form -->
          {!! Form::open(array( 'route' => 'account-createPost', 'method'=>'POST','class' => 'form', 'files' => true)) !!}
          <div class="row order-area order-middle">
            <div class="col-sm-6">
  
          	 <div class="form-group">
                <label for="usr">Email Address :</label>
                {!! Form::email('emailId', $value = null, $attributes = ['class'=>'form-control', 'id'=>'emailId',
                	'placeholder'=>'Email Address',/* 'autocomplete'=>'off',*/ 'required'=>'required']); !!}
              </div>
              
              <div class="form-group">
                <label for="usr">User Name :</label>
                {!! Form::text('userName', $value = null, $attributes = ['class'=>'form-control', 'id'=>'userName',
                	'placeholder'=>'User Name',/* 'autocomplete'=>'off',*/ 'required'=>'required']); !!}
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label for="usr">Mobile no:</label>
                {!! Form::text('mobileNumber','',['name'=>'mobileNumber','class'=>'form-control', 'id'=>'mobileNumber',
                	'placeholder'=>' 10 digit Mobile Number','pattern'=>'^[6789]\d{9}$',/* 'autocomplete'=>'off',*/ 'maxlength'=>'10', 'required'=>'required']) !!}
            </div>
               <div class="form-group">
                
              </div>             
              <div class="form-group">
                <label for="usr">Password :</label>
                {!! Form::text('password','',['name'=>'password','class'=>'form-control', 'id'=>'password', 
                	'placeholder'=>' Max 10 letter  ',/*'autocomplete'=>'off',*/ 'maxlength'=>'10', 'required'=>'required']) !!}

              </div>
              
                
                       
              
            </div>
            <div class="clearfix"></div>
            <button class="borderBtn marginT15" type="submit">Save</button> 
          </div>
         {!! Form::close() !!}
         <!-- start of end of form -->
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </section>

<script>
	var AccountgenerateOTPUrl="{!!route('account-verifybyotp')!!}";
</script>

@endsection
