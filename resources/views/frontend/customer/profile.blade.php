@extends('frontend/layout/1-column')
@section('content')
<!-- start of profile edit page -->

<section class="product-area">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 account-area">
          <h2 class="order-text">Profile</h2>
          <div class="clearfix"></div>
          <!-- start of Form -->
          {!! Form::open(array( 'route' => 'account-saveprofiledata', 'method'=>'POST','class' => 'form', 'files' => true)) !!}
          <div class="row order-area order-middle">
            <div class="col-sm-6">
              <div class="form-group">
                <label for="usr">First Name :</label>
                {!! Form::text('firstName','',['name'=>'firstName','class'=>'form-control', 'id'=>'firstName', 
                	'placeholder'=>'First Name','autocomplete'=>'off', 'required'=>'required']) !!}
              </div>
              <div class="form-group">
                <label for="usr">Middle Name :</label>
                {!! Form::text('middleName','',['name'=>'middleName','class'=>'form-control', 'id'=>'middleName', 
                	'placeholder'=>'Middle Name','autocomplete'=>'off']) !!}
              </div>              
              <div class="form-group">
                <label for="usr">Last Name:</label>
                {!! Form::text('lastName','',['name'=>'lastName','class'=>'form-control', 'id'=>'lastName', 
                	'placeholder'=>'Last Name','autocomplete'=>'off', 'required'=>'required']) !!}
               </div>
          	 <div class="form-group">
                <label for="usr">Email Address :</label>
                {!! Form::email('defaultEmailId', $value = null, $attributes = ['class'=>'form-control', 'id'=>'defaultEmailId',
                	'placeholder'=>'Email Address','autocomplete'=>'off', 'required'=>'required']); !!}
              </div>
              <div class="form-group">
                <label for="usr">User Name :</label>
                {!! Form::email('userName', $value = null, $attributes = ['class'=>'form-control', 'id'=>'userName',
                	'placeholder'=>'User Name','autocomplete'=>'off', 'required'=>'required']); !!}
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label for="usr">Mobile no:</label>
                {!! Form::text('defaultMobileNumber','',['name'=>'defaultMobileNumber','class'=>'form-control', 'id'=>'defaultMobileNumber', 
                	'placeholder'=>' 10 digit Mobile Number','autocomplete'=>'off', 'maxlength'=>'10', 'required'=>'required', 'pattern' => '[0-9]+']) !!}
              </div>
              <div class="form-group">
                <label for="usr">Password :</label>
                 {!! Form::password('password',array('class' => 'form-control')) !!}

              </div>
              
              <div class="form-group">
                <label for="usr">Gender</label>
                {!! Form::select('gender', array('MALE' => 'Male', 'FEMALE' => 'Female'), 'MALE',['class'=>'form-control', 'id'=>'gender']); !!}
              </div>  
                       
              <div class="form-group">
                <label for="usr">Profile Photo :</label>
               
                  {!!  Form::file('profilePhoto','',array('id'=>'profilePhoto','class'=>'form-control'))!!}
              </div>
            </div>
            <div class="clearfix"></div>
            <button class="borderBtn marginT15" type="submit">Save</button> 
          </div>
         {!! Form::close() !!}
         <!-- start of end of form -->
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </section>



@endsection
