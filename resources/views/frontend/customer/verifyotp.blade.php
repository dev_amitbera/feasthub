<?php
/**
 * Created by dev.amitbera@gmail.com
 * User: Amit
 */
?>
@extends('frontend/layout/1-column')
@section('content')
    <!-- start of profile edit page -->

    <section class="product-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 account-area">
                    <h2 class="order-text">Verify Otp</h2>
                    <div class="clearfix"></div>
                    <!-- start of Form -->
                    {!! Form::open(array( 'route' => 'account-verifybyotp', 'method'=>'POST','class' => 'form', 'files' => true)) !!}
                    <div class="row order-area order-middle ">
                        <div class="col-sm-6" >
                            <div class="form-group">
                                <label for="usr">Enter otp :</label>
                                {!! Form::text('oneTimePassword','',['name'=>'oneTimePassword','class'=>'form-control', 'id'=>'firstName',
                                    'placeholder'=>'Put your otp','autocomplete'=>'off', 'required'=>'required']) !!}
                            </div>
                            <div class="form-group">
                    {!! Form::submit('Verify your mobile no',['name'=>'VerifyMobileNumber', 'id'=>'VerifyMobileNumber', 'class'=>'borderBtn marginT15']) !!}
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                    </div>
                {!! Form::close() !!}
                <!-- start of end of form -->
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </section>

    <script>
        var AccountgenerateOTPUrl="{!!route('account-verifybyotp')!!}";
    </script>

@endsection
