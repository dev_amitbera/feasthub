@extends('frontend/layout/1-column')
@section('content')
<!-- start of profile edit page -->

<section class="product-area">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 account-area">
          <h2 class="order-text">Change your password</h2>
          <div class="clearfix"></div>
          <!-- start of Form -->
          {!! Form::open(array( 'route' => 'account-ChangePasswordSave', 'method'=>'POST','class' => 'form', 'files' => true)) !!}
          <div class="row order-area order-middle">
            <div class="col-sm-6">
              <div class="form-group">
                <label for="usr">Current Password</label>
                {!! Form::text('password','',['name'=>'password','class'=>'form-control', 'id'=>'password', 
                	'placeholder'=>'Current Password','autocomplete'=>'off', 'required'=>'required']) !!}
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label for="usr">New Password</label>
                {!! Form::text('newPassword','',['name'=>'newPassword','class'=>'form-control', 'id'=>'newPassword', 
                	'placeholder'=>' New password','autocomplete'=>'off', 'maxlength'=>'10', 'required'=>'required']) !!}
              </div>
            </div>
            <div class="clearfix"></div>
            <button class="borderBtn marginT15" type="submit">Change Password</button> 
          </div>
         {!! Form::close() !!}
         <!-- start of end of form -->
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </section>

@endsection
