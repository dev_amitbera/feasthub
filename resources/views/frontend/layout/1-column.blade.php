<?php
/**
 * Created by PhpStorm.
 * User: Amit
 * Date: 6/23/2016
 * Time: 11:17 PM
 */?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Profile</title>

<!-- Stylesheets Desktop -->
<link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('frontend/css/font-awesome.min.css') }}" />
<link rel="stylesheet" href="{{ asset('frontend/css/main.css') }}" />
<link rel="stylesheet" href="{{ asset('frontend/css/side-navbar.css') }}" />
<link rel="stylesheet" href="{{ asset('frontend/css/side-navbar.css') }}" />
<link rel="stylesheet" href="{{ asset('frontend/css/responsive.css') }}" />
<link rel="stylesheet" href="{{ asset('frontend/css/notification.css') }}" />

<script src="{{ asset('frontend/js/jquery-2.1.4.min.js') }}"></script>
<script>
$(document).ready(function(){
    $(".nav-tabs a").click(function(){
        $(this).tab('show');
    });
    $('.nav-tabs a').on('shown.bs.tab', function(event){
        var x = $(event.target).text();         // active tab
        var y = $(event.relatedTarget).text();  // previous tab
        $(".act span").text(x);
        $(".prev span").text(y);
    });
});
</script>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="{{ asset('frontend/js/html5shiv.js') }}"></script>
  <script src="{{ asset('frontend/js/respond.min.js') }}"></script>
<![endif]-->
    <script src="{{ asset('frontend/js/account.js') }}"></script>
    <script src="{{ asset('frontend/js/account.js') }}"></script>
    <script src="{{ asset('frontend/js/notification.js') }}"></script>

</head>

<body>
<div id="wrapper"> 
  <!--================= start header ======================-->
   @include('frontend.include.header')
    @include('includes.notification')
  <!--================= start header ======================--> 
  
  <!--================= start body ======================-->
    @yield('content')
  
  <!--================= END body ======================-->
  
  
@include('frontend.include.footer')
</div>
</div>
<!-- Bootstrap core JavaScript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script> 
<script type="text/javascript" src="{{ asset('frontend/js/main.js') }}"></script>
</body>
</html>

