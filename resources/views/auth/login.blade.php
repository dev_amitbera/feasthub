@extends('layouts.frontend')
@section('content')
<!-- start of profile edit page -->

<section class="product-area">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 account-area">
          <h2 class="order-text">Sign In</h2>
          <div class="clearfix"></div>          
			@if($errors->any())			
				<ul class="alert alert-danger">
					@foreach($errors->all() as $error)
						<li>{{$error}}</li>
					@endforeach
				</ul>
			@endif	
			<div class="clearfix"></div>			
          <!-- start of Form -->
          {!! Form::open(['route'=>'login', 'method'=>'POST','class' => 'form', 'files' => true]) !!}
          <div class="row order-area order-middle">
            <div class="col-sm-6">
  
          	 <div class="form-group">
                <label for="usr">E-Mail Address</label>
                {!! Form::email('email', $value = old('email'), $attributes = ['class'=>'form-control', 'id'=>'email',
                	'placeholder'=>'Email Address',/* 'autocomplete'=>'off',*/ 'required'=>'required']); !!}
              </div>          
              <div class="form-group">
                <label for="usr">Password :</label>
                {!! Form::text('password','',['name'=>'password','class'=>'form-control', 'id'=>'password', 
                	'placeholder'=>' Max 20 letter  ',/*'autocomplete'=>'off',*/ 'maxlength'=>'12', 'required'=>'required']) !!}

              </div>
            </div>
            <div class="clearfix"></div>
            <button class="borderBtn marginT15" type="submit">Save</button> 
          </div>
         {!! Form::close() !!}
         <!-- start of end of form -->
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </section>

<script>
	var AccountgenerateOTPUrl="{!!route('account-verifybyotp')!!}";
</script>

@endsection
