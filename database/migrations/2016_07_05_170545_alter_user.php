<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('users', function($table) {
			$table->string('defaultEmailId','255')->nullable();
			$table->string('defaultLandlineNumber','255')->nullable();
			$table->string('defaultMobileNumber','255')->nullable();
			$table->string('emailVerifiedFlag','255')->nullable();
			$table->string('firstName','255')->nullable();
			$table->string('gender','255')->nullable();
			$table->string('languageCode','255')->nullable();
			$table->string('lastName','255')->nullable();
			$table->string('middleName','255')->nullable();
			$table->string('mobileVerifiedFlag','255')->nullable();
			$table->string('api_password','255')->nullable();
			$table->string('primaryRole','255')->nullable();
			$table->string('profilePhoto','255')->nullable();
			$table->string('referralId','255')->nullable();
			$table->string('registeredCountryCode','255')->nullable();
			$table->string('salutation','255')->nullable();
			$table->string('subscriptionFlag','255')->nullable();
			$table->string('userAuthMethod','255')->nullable();
			$table->string('userName','255')->nullable();
			$table->string('userType','255')->nullable();
			
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
			$table->dropColumn('defaultEmailId');
			$table->dropColumn('defaultLandlineNumber');
			$table->dropColumn('defaultMobileNumber');
			$table->dropColumn('emailVerifiedFlag');
			$table->dropColumn('firstName');
			$table->dropColumn('gender');
			$table->dropColumn('languageCode');
			$table->dropColumn('lastName');
			$table->dropColumn('middleName');
			$table->dropColumn('mobileVerifiedFlag');
			$table->dropColumn('api_password');
			$table->dropColumn('primaryRole');
			$table->dropColumn('profilePhoto');
			$table->dropColumn('referralId');
			$table->dropColumn('registeredCountryCode');
			$table->dropColumn('salutation');
			$table->dropColumn('subscriptionFlag');
			$table->dropColumn('userAuthMethod');
			$table->dropColumn('userName');
			$table->dropColumn('userType');
		});
    }
}
