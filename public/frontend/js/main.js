
$(document).ready(function () { var trigger = $('.hamburger'), overlay = $('.overlay'), isClosed = false; trigger.click(function () { hamburger_cross(); }); function hamburger_cross() { if (isClosed == true) { overlay.hide(); trigger.removeClass('is-open'); trigger.addClass('is-closed'); isClosed = false; } else { overlay.show(); trigger.removeClass('is-closed'); trigger.addClass('is-open'); isClosed = true; } } $('[data-toggle="offcanvas"]').click(function () { $('#wrapper').toggleClass('toggled'); }); });

if ($('#back-to-top').length) {
    var scrollTrigger = 100, // px
        backToTop = function () {
            var scrollTop = $(window).scrollTop();
            if (scrollTop > scrollTrigger) {
                $('#back-to-top').addClass('show');
            } else {
                $('#back-to-top').removeClass('show');
            }
        };
    backToTop();
    $(window).on('scroll', function () {
        backToTop();
    });
    $('#back-to-top').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 700);
    });
}

//plugin bootstrap minus and plus
//http://jsfiddle.net/laelitenetwork/puJ6G/
$( document ).ready(function() {
    $('.btn-number').click(function(e){
        e.preventDefault();
        
        var fieldName = $(this).attr('data-field');
        var type      = $(this).attr('data-type');
        var input = $("input[name='"+fieldName+"']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if(type == 'minus') {
                var minValue = parseInt(input.attr('min')); 
                if(!minValue) minValue = 1;
                if(currentVal > minValue) {
                    input.val(currentVal - 1).change();
                } 
                if(parseInt(input.val()) == minValue) {
                    $(this).attr('disabled', true);
                }
    
            } else if(type == 'plus') {
                var maxValue = parseInt(input.attr('max'));
                if(!maxValue) maxValue = 9999999999999;
                if(currentVal < maxValue) {
                    input.val(currentVal + 1).change();
                }
                if(parseInt(input.val()) == maxValue) {
                    $(this).attr('disabled', true);
                }
    
            }
        } else {
            input.val(0);
        }
    });
    $('.input-number').focusin(function(){
       $(this).data('oldValue', $(this).val());
    });
    $('.input-number').change(function() {
        
        var minValue =  parseInt($(this).attr('min'));
        var maxValue =  parseInt($(this).attr('max'));
        if(!minValue) minValue = 1;
        if(!maxValue) maxValue = 9999999999999;
        var valueCurrent = parseInt($(this).val());
        
        var name = $(this).attr('name');
        if(valueCurrent >= minValue) {
            $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if(valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        
        
    });
    $(".input-number").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                 // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) || 
                 // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
    });
});	


$( document ).ready(function() {
    $('.category-list').click(function(e){
        e.preventDefault();
		var catId = $(this).attr('data-category-id');
		var _token = $(this).attr('data-token');
		var postData = {"catId":catId, "_token":_token };
        var formURL = $(this).attr('data-category-url');;   
		$.ajax({
					 type: "POST",
					 url: formURL,
					 data: postData,
					 dataType: 'json',
					 success: function(result)
					           {
								 if(result.success==true){
									 location.reload();
								 }
								 else{
								  alert('No products available!');
								 }
							   } 
					});
    });
	$('.product-list').click(function(e){
        e.preventDefault();
		var productId = $(this).attr('data-product-id');
		var dataId = $(this).attr('data-id');
		var _token = $(this).attr('data-token');
		var postData = {"productId":productId,"dataId":dataId, "_token":_token };
        var formURL = $(this).attr('data-product-url'); 
		$.ajax({
					 type: "POST",
					 url: formURL,
					 data: postData,
					 dataType: 'json',
					 success: function(result)
					           {
								 if(result.success==true){
									 location.reload();
								 }
								 else{
								  alert('No product information available!');
								 }
							   } 
					});
    });
});	
