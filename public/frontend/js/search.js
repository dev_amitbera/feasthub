// JavaScript Document
    var autocomplete;
    function initialize() {
	 var options = {componentRestrictions: {country: "IN"}};
	 var input = document.getElementById('searcharea');
	 autocomplete = new google.maps.places.Autocomplete(input, options);
	 autocomplete.addListener('place_changed', fillInAddress);
	}
    google.maps.event.addDomListener(window, 'load', initialize);
	var selected_locality_lat = null;
	var selected_locality_lng = null;
	var selected_locality_name = null;

	function fillInAddress() {
	  $("#load_menu").removeAttr('disabled');
	  var place = autocomplete.getPlace();
	  selected_locality_name = place.address_components[0].long_name;
	  selected_locality_lat = place.geometry.location.lat();
	  selected_locality_lng = place.geometry.location.lng();
	}

    $(document).ready(function () {
	    $("#searcharea").attr('disabled', 'disabled');
		$("#load_menu").attr('disabled', 'disabled');
		$('#city option').each(function(){
		  if($(this).val()==''){
			 $(this).attr('disabled', 'disabled');
			 $(this).attr('id', 'sel_opt_1'); 
		   }
		});
    });
	function geocodeCity(city) {
	  var geocoder = new google.maps.Geocoder();
	  geocoder.geocode({
		'address': city
	  }, function (results, status) {
		if (status === google.maps.GeocoderStatus.OK) {
		  autocomplete.setBounds(results[0].geometry.bounds);
		} else {
		  geolocate();
		}
	  })
	}
	$(document).on('change', '#city', function () {
	  geocodeCity($(this).val());
	  $('.orderHomePageCTA h3').text('Tasty food delivered to you across ' + $(this).val());
	  $("#sel_opt_1").remove();
	  $("#searcharea").removeAttr('disabled');
	  $("#searcharea").val('');
    });
$(document).on("click", "#load_menu", function () {
	$("#selected_locality_name").val(selected_locality_name);
	$("#selected_locality_lat").val(selected_locality_lat);
	$("#selected_locality_lng").val(selected_locality_lng);
	$("#searchForm").submit() ;
});
